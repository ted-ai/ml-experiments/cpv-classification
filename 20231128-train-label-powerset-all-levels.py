import pickle
import sys

import optuna
import pandas as pd
from pandas import DataFrame
from sklearn import model_selection, metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from skmultilearn.problem_transform import LabelPowerset

LOCAL = False
TRIAL_COUNT = 2 if LOCAL else 20
NROWS = 10000 if LOCAL else None
MAX_LEVELS = list((range(2, 3) if LOCAL else range(1, 5)))


def evaluate_model(trial: optuna.Trial, model: Pipeline, df: pd.DataFrame, cpvs: list[str], df_name: str) -> float:
    predicted_df = model.predict(df["text"])
    scores = metrics.precision_recall_fscore_support(df[cpvs], predicted_df, average="micro")
    precision, recall, f1_score, _ = scores
    report = metrics.classification_report(df[cpvs], predicted_df, target_names=cpvs)
    trial.set_user_attr(f"{df_name}_precision", precision)
    trial.set_user_attr(f"{df_name}_recall", recall)
    trial.set_user_attr(f"{df_name}_f1_score", f1_score)
    trial.set_user_attr(f"{df_name}_report", report)
    return f1_score


def train_step(trial: optuna.Trial, train_df: DataFrame, test_df: DataFrame, cpvs: list[str]) -> float:
    vectorizer = TfidfVectorizer(
        ngram_range=(1, trial.suggest_int("max_ngram_bound", 2, 3)),
        min_df=trial.suggest_int("min_df", 1, 3),
        max_df=trial.suggest_float("max_df", 0.1, 1.0),
    )
    classifier = LabelPowerset(require_dense=[False, False], classifier=LinearSVC(
        max_iter=10000,
        C=trial.suggest_float("C", 1, 20),
        random_state=0,
        dual=True,
    ))
    model = Pipeline([("tfidf", vectorizer), ("clf", classifier)])
    model.fit(train_df["text"], train_df[cpvs])
    trial.set_user_attr("model_size", sys.getsizeof(pickle.dumps(model)))
    evaluate_model(trial, model, train_df, cpvs, "train")
    return evaluate_model(trial, model, test_df, cpvs, "test")


def train_division(df: DataFrame, max_level: int):
    train_df, test_df = model_selection.train_test_split(df, test_size=0.2, random_state=0)
    max_cpv_length = max_level + 1
    levels_label = ''.join(map(str, range(1, max_level + 1)))
    cpvs = [c for c in df.columns if len(c) <= max_cpv_length and c.isdigit()]
    print(f"Start training on levels {levels_label} ({len(cpvs)} codes)...")
    study = optuna.create_study(direction="maximize")
    study.optimize(lambda trial: train_step(trial, train_df, test_df, cpvs), n_trials=TRIAL_COUNT)
    study.trials_dataframe().to_csv(f"tmp/20231128-label-powerset-lvl{levels_label}-trials.csv", index=False)
    print(f"Training on levels {levels_label} done.")


def main():
    df = pd.read_csv("tmp/20231127-en-dataset-lvl1234.zip", dtype={"id": str, "text": str}, nrows=NROWS)
    for max_level in MAX_LEVELS:
        train_division(df, max_level)
    print("Done")


if __name__ == '__main__':
    main()
