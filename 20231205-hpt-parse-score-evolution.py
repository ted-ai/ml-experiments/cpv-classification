import re
from dataclasses import dataclass
from pathlib import Path

from dataclass_csv import DataclassWriter

# RESULT_FOLDER folder has been created manually.
# It contains the following files:
#   - logs-lvl1.log
#   - logs-lvl12.log
#   - logs-lvl123.log
#   - logs-lvl1234.log
# These files are the logs of the training job script in 20231129-hpt folder.


LEVELS = ["1", "12", "123", "1234"]
RESULT_FOLDER = Path("tmp") / "20231205-pth-results"
LOG_FILE = "logs-lvl{}.log"
EPOCH_FILE = "epochs-lvl{}.csv"
EPOCH_PATTERN = re.compile(r"loss:.*\[(?P<training_time>[0-9:]+).*\n"
                           r".*\[(?P<validation_time>[0-9:]+).*\n"
                           r"macro [0-9.]+ micro (?P<micro_score>[0-9.]+)",
                           re.MULTILINE)


@dataclass
class Epoch:
    training_time_secs: int
    validation_time_secs: int
    micro_score: float


def main():
    for levels in LEVELS:
        lines = [
            line
            for line in (RESULT_FOLDER / LOG_FILE.format(levels)).read_text().splitlines()
            if "%|" not in line or "100%" in line
        ]
        lines = [
            lines[i]
            for i in range(len(lines))
            if lines[i].startswith("macro") or i == 0 or "100%" not in lines[i - 1]
        ]
        matches = EPOCH_PATTERN.findall("\n".join(lines))
        epochs = [
            Epoch(
                training_time_secs=to_secs(training_time),
                validation_time_secs=to_secs(validation_time),
                micro_score=float(micro_score)
            )
            for (training_time, validation_time, micro_score) in matches
        ]
        with (RESULT_FOLDER / EPOCH_FILE.format(levels)).open(mode='w', newline='') as file:
            writer = DataclassWriter(file, epochs, Epoch)
            writer.write()


def to_secs(duration: str) -> int:
    components = [int(x) for x in duration.split(':')]
    if len(components) == 3:
        return components[0] * 60 * 60 + components[1] * 60 + components[2]
    else:
        return components[0] * 60 + components[1]


if __name__ == '__main__':
    main()
