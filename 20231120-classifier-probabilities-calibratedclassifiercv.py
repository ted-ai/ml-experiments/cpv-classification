import pickle
import sys

import optuna
import pandas as pd
from imblearn.pipeline import Pipeline
from sklearn.calibration import CalibratedClassifierCV
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import coverage_error, classification_report
from sklearn.metrics import f1_score, roc_auc_score, accuracy_score
from sklearn.metrics import label_ranking_average_precision_score
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC

ALL_DIVISIONS = ['85', '44', '50', '80', '73', '45', '71', '79', '90', '30', '35', '33', '55', '72', '48', '38', '09',
                 '75', '66', '64', '42', '34', '60', '92', '39', '31', '98', '51', '32', '65', '77', '22', '63', '15',
                 '70', '18', '03', '24', '43', '19', '41', '37', '14', '16', '76']


# adapted from: https://jesusleal.io/2021/04/21/Longformer-multilabel-classification/
def multi_label_metrics(ytest, y_pred):
    y_true = ytest
    f1_micro_average = f1_score(y_true=y_true, y_pred=y_pred, average='micro')
    roc_auc = roc_auc_score(y_true, y_pred, average='micro')
    accuracy = accuracy_score(y_true, y_pred)
    coverage_err = coverage_error(y_true, y_pred)
    label_ranking_average_precision = label_ranking_average_precision_score(y_true, y_pred)
    return {
        'f1': f1_micro_average,
        'roc_auc': roc_auc,
        'accuracy': accuracy,
        'coverage_error': coverage_err,
        'label_ranking_average_precision_score': label_ranking_average_precision
    }


def objective(trial: optuna.Trial):
    x = df['title_texte'].values.astype(str)
    y = df[ALL_DIVISIONS]
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    model = Pipeline([
        ('tfidf', TfidfVectorizer(
            ngram_range=(1, trial.suggest_int('max_ngram', 3, 3)),
            min_df=trial.suggest_int('min_df', 1, 1),
            max_df=trial.suggest_float('max_df', 0.2063, 0.2063),
        )),
        ('clf', OneVsRestClassifier(n_jobs=-1, estimator=CalibratedClassifierCV(cv=5, estimator=LinearSVC(
            C=trial.suggest_float('C', 7.9165, 7.9165),
            max_iter=10000,
            random_state=0,
            dual=True,
        )))),
    ])
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    metrics = multi_label_metrics(y_test, y_pred)
    report = classification_report(y_test, y_pred, target_names=ALL_DIVISIONS)
    trial.set_user_attr('f1', metrics['f1'])
    trial.set_user_attr('roc_auc', metrics['roc_auc'])
    trial.set_user_attr('accuracy', metrics['accuracy'])
    trial.set_user_attr('coverage_error', metrics['coverage_error'])
    trial.set_user_attr('label_ranking_average_precision_score', metrics['label_ranking_average_precision_score'])
    trial.set_user_attr('report', report)
    trial.set_user_attr("model_size", sys.getsizeof(pickle.dumps(model)))
    return metrics['f1']


if __name__ == '__main__':
    # Dataset available at s3://d-ew1-ted-ai-ml-data/experiments/roberta_cpv_v0.0.1/20231020-dataset_formatted_with_title_and_short_desription_combined-all-EN-notices-ted_with_preprocessing.csv
    df = pd.read_csv(
        "tmp/20231020-dataset_formatted_with_title_and_short_desription_combined-all-EN-notices-ted_with_preprocessing.csv",
        index_col=0
    )
    df['title_texte'] = df['processed_text']
    df = df.drop(['year', 'processed_text'], axis=1)
    study = optuna.create_study(direction="maximize")
    study.optimize(objective, n_trials=1)
    results_df = study.trials_dataframe()
    results_df.to_csv("tmp/20231120-results_optuna_calibratedclassifiercv.csv")
    print("Best trial:", study.best_trial)
