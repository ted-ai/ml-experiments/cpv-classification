from __future__ import print_function

import argparse
import joblib
import os
import re
import json

from unidecode import unidecode
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score, roc_auc_score, accuracy_score, coverage_error, label_ranking_average_precision_score
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
import spacy.cli


spacy.cli.download("en_core_web_sm")
NLP = spacy.load("en_core_web_sm")
STOP_WORDS = NLP.Defaults.stop_words
CHARACTERS_TO_REPLACE = ["\\n", "\\r", "\\t", "\\W", "•", "\t", "-", "(", ")", ":", ";", "?", "!", "&", "\n", "\r", ".", ",", "'", "’", "´",
                         "‘", "’", '"', "“", "”", "'", "/", "\\", "%", "—", "#", "$", "[", "]", "|", "{", "}", "~", "`", "+", "*"]

MONTHS = [" january ", " february ", " march ", " april ", " may ", " june ", " july ", " august ", " september ", " october ", " november ", " december ",
          " jan ", " feb ", " mar ", " apr ", " jun ", " jul ", " aug ", " sep ", " oct ", " nov ", " dec "]


ALL_CPVS = ['85', '44', '50', '80', '73', '45', '71', '79', '90', '30', '35', '33', '55', '72', '48', '38', '09', '75', '66', '64', '42', '34', '60', '92', '39', '31', '98', '51', '32', '65', '77', '22', '63', '15', '70', '18', '03', '24', '43', '19', '41', '37', '14', '16', '76']
          
def _remove_multiple_spaces(text: str) -> str:
    return re.sub('\s+', ' ', text)


def _remove_special_characters(text: str) -> str:
    for chars in CHARACTERS_TO_REPLACE:
        text = text.replace(chars, " ")
    return text


def _remove_stop_words(text: str) -> str:
    token_list = text.split()
    removed_list = [x for x in token_list if x not in STOP_WORDS]
    return ' '.join(removed_list)


def _replace_digits(text):
    return re.sub(r'[\d-]+', 'NUMBER', text)


def _delete_one_letter_word(text):
    text_as_list = text.split()
    text_as_list = [element for element in text_as_list if len(element) > 1]
    return ' '.join(text_as_list)


def _remove_consecutive_duplicates(text):
    text_as_list = text.split()
    last_seen = None
    result = []
    for x in text_as_list:
        if x != last_seen:
            result.append(x)
        last_seen = x
    return ' '.join(result)

          
def _replace_months(text: str) -> str:
    text = " " + text + " "
    for month in MONTHS:
        text = text.replace(month, " MONTH ")
    return text


def _replace_with_lemma(text: str) -> str:
    doc = NLP(text)
    lemmatized_list = []
    for token in doc:
        lemmatized_list.append(token.lemma_)
    return " ".join(lemmatized_list)


def _preprocess_input(text: str) -> str:
    x = unidecode(str(text).lower())
    x = _replace_with_lemma(x)
    x = _remove_special_characters(x)
    x = _remove_stop_words(x)
    x = _remove_multiple_spaces(x)
    x = _replace_digits(x)
    x = _delete_one_letter_word(x)
    x = _remove_consecutive_duplicates(x)
    x = _replace_months(x)
    return x
    

def multi_label_metrics(y_true,y_pred):
    f1_micro_average = f1_score(y_true=y_true, y_pred=y_pred, average='micro')
    roc_auc = roc_auc_score(y_true, y_pred, average = 'micro')
    accuracy = accuracy_score(y_true, y_pred)
    coverage_err = coverage_error(y_true, y_pred)
    label_ranking_average_precision = label_ranking_average_precision_score(y_true, y_pred)
    print(f'f1={f1_micro_average};')
    print(f'accuracy={accuracy};')
    print(f'coverage_err={coverage_err};')
    print(f'label_ranking_average_precision={label_ranking_average_precision};')  

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Hyperparameters are described here.
    parser.add_argument('--c_param', type=int, default=20)

    # Sagemaker specific arguments. Defaults are set in the environment variables.
    parser.add_argument('--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])
    parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_TEST'])

    args = parser.parse_args()

    # Load train data
    input_train_files = [ os.path.join(args.train, file) for file in os.listdir(args.train) ]
    if len(input_train_files) == 0:
        raise ValueError(('There are no files in {}.\n' +
                          'This usually indicates that the channel ({}) was incorrectly specified,\n' +
                          'the data specification in S3 was incorrectly specified or the role specified\n' +
                          'does not have permission to access the data.').format(args.train, "train"))
    train_raw_data = [ pd.read_csv(file, index_col=0, engine="python") for file in input_train_files ]
    train_data = pd.concat(train_raw_data)
    X_train = train_data['title_texte']
    y_train = train_data.drop(['title_texte'], axis=1)
                        
    # Load test data 
    input_test_files = [ os.path.join(args.test, file) for file in os.listdir(args.test) ]
    if len(input_test_files) == 0:
        raise ValueError(('There are no files in {}.\n' +
                          'This usually indicates that the channel ({}) was incorrectly specified,\n' +
                          'the data specification in S3 was incorrectly specified or the role specified\n' +
                          'does not have permission to access the data.').format(args.train, "train"))
    test_raw_data = [ pd.read_csv(file, index_col=0, engine="python") for file in input_test_files ]
    test_data = pd.concat(test_raw_data)

    X_test = test_data['title_texte']
    y_test = test_data.drop(['title_texte'], axis=1)
                
    # Train model            
    c_param = args.c_param
    SVC_pipeline = Pipeline([
                ('tfidf', TfidfVectorizer(ngram_range=(1, 3))),
                ('clf', OneVsRestClassifier(LinearSVC(max_iter=10000, C=c_param, random_state=736283))),
            ])
    SVC_pipeline.fit(X_train, y_train)
                        
    # Evaluate model
    y_pred = SVC_pipeline.predict(X_test)
    multi_label_metrics(y_test, y_pred)
    joblib.dump(SVC_pipeline, os.path.join(args.model_dir, "model.joblib"))


# def input_fn(request_body, request_content_type):
#     #body = json.loads(request_body)
#     print(request_body)
#     title = request_body.get("title", "")
#     description = request_body.get("description", "")
#     if not title and not description:
#         raise Exception("No title and description provided")
#     merged_title_description = f"{title}. {description}"
#     preprocessed_input = _preprocess_input(merged_title_description)
#     return [preprocessed_input]


def predict_fn(input_object, model):
    preprocessed_input = [_preprocess_input(element) for element in input_object]
    predictions = model.predict(preprocessed_input)
    results = []
    for prediction in predictions:
        local_result = []
        for index, element in enumerate(prediction):
            if element:
                local_result.append(ALL_CPVS[index])
        results.append(local_result)
    return results
    
# def output_fn(predictions, content_type):
#     results = []
#     for prediction in predictions:
#         local_result = []
#         for index, element in enumerate(prediction):
#             if element:
#                 local_result.append(ALL_CPVS[index])
#         results.append(local_result)
#     return results
                           
                           
def model_fn(model_dir):
    SVC_pipeline = joblib.load(os.path.join(model_dir, "model.joblib"))
    return SVC_pipeline
