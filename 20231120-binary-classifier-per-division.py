import math
import pickle
import sys
from multiprocessing import Pool
from pathlib import Path

import optuna
import pandas as pd
from sklearn import model_selection, metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC


def list_divisions(df: pd.DataFrame) -> list[str]:
    return [c for c in df.columns if len(c) == 2 and c.isdigit()]


def list_groups(df: pd.DataFrame, div: str) -> list[str]:
    return [c for c in df.columns if len(c) == 3 and c.startswith(div)]


def balance_df(df: pd.DataFrame, pos_div: str, neg_div_ratio: float) -> pd.DataFrame:
    divisions = list_divisions(df)
    pos_count = (df[pos_div] == 1).sum()
    neg_count_per_div = pos_count * neg_div_ratio
    neg_divisions = [div for div in divisions if div != pos_div]
    neg_dfs = [filter_neg_div_df(df, div, neg_count_per_div) for div in neg_divisions]
    pos_df = df[df[pos_div] == 1]
    balanced_df = pd.concat(neg_dfs + [pos_df])
    return balanced_df.drop_duplicates()


def filter_neg_div_df(df: pd.DataFrame, neg_div: str, neg_count: int) -> pd.DataFrame:
    groups = list_groups(df, neg_div)
    count_per_group = math.ceil(neg_count / len(groups))
    return pd.concat([df[df[group] == 1].head(count_per_group) for group in groups])


def evaluate_model(trial: optuna.Trial, model: Pipeline, pos_div: str, df: pd.DataFrame, df_name: str) -> float:
    predicted_df = model.predict(df["text"])
    scores = metrics.precision_recall_fscore_support(df[pos_div], predicted_df, average="binary")
    precision, recall, f1_score, _ = scores
    confusion_matrix = metrics.confusion_matrix(df[pos_div], predicted_df)
    trial.set_user_attr(f"{df_name}_precision", precision)
    trial.set_user_attr(f"{df_name}_recall", recall)
    trial.set_user_attr(f"{df_name}_f1_score", f1_score)
    trial.set_user_attr(f"{df_name}_true_negative_count", int(confusion_matrix[0][0]))
    trial.set_user_attr(f"{df_name}_false_positive_count", int(confusion_matrix[0][1]))
    trial.set_user_attr(f"{df_name}_false_negative_count", int(confusion_matrix[1][0]))
    trial.set_user_attr(f"{df_name}_true_positive_count", int(confusion_matrix[1][1]))
    return f1_score


def highest_neg_div_ratio(df: pd.DataFrame, pos_div: str) -> float:
    max_div_size = 0
    for neg_div in (div for div in list_divisions(df) if div != pos_div):
        groups = list_groups(df, neg_div)
        max_group_size = max((df[group] == 1).sum() for group in groups)
        max_div_size = max(max_div_size, max_group_size * len(groups))
    return max_div_size / (df[pos_div] == 1).sum()


def train_step(trial: optuna.Trial, pos_div: str) -> float:
    df = pd.read_csv("tmp/20231031-en-dataset-lvl12.csv", dtype={"id": str, "text": str})
    train_df, test_df = model_selection.train_test_split(df, test_size=0.2, random_state=0)
    del df
    max_neg_div_ratio = highest_neg_div_ratio(train_df, pos_div)
    trial.set_user_attr("max_neg_div_ratio", max_neg_div_ratio)
    neg_div_ratio = trial.suggest_float("neg_div_ratio", 0.1, max_neg_div_ratio)
    final_train_df = balance_df(train_df, pos_div, neg_div_ratio)
    del train_df
    vectorizer = TfidfVectorizer(
        ngram_range=(1, trial.suggest_int("max_ngram_bound", 2, 3)),
        min_df=trial.suggest_int("min_df", 1, 3),
        max_df=trial.suggest_float("max_df", 0.1, 1.0),
    )
    classifier = LinearSVC(
        max_iter=10000,
        C=trial.suggest_float("C", 1, 20),
        random_state=0,
        dual=True,
    )
    model = Pipeline([("tfidf", vectorizer), ("clf", classifier)])
    model.fit(final_train_df["text"], final_train_df[pos_div])
    trial.set_user_attr("model_size", sys.getsizeof(pickle.dumps(model)))
    evaluate_model(trial, model, pos_div, final_train_df, "train")
    del final_train_df
    return evaluate_model(trial, model, pos_div, test_df, "test")


def train_division(division: str):
    print(f"Start training division {division}...")
    study = optuna.create_study(direction="maximize")
    study.optimize(lambda trial: train_step(trial, division), n_trials=50)
    study.trials_dataframe().to_csv(f"tmp/20231120_training_results/trials_{division}.csv", index=False)
    print(f"Training of division {division} done.")


def main():
    Path("tmp/20231120_training_results").mkdir(exist_ok=True)
    all_divisions = list_divisions(pd.read_csv("tmp/20231031-en-dataset-lvl12.csv"))
    with Pool(45) as pool:
        pool.map(train_division, all_divisions)
    print("Done")


if __name__ == '__main__':
    main()
