import json
import os
import sys

import psycopg2
from psycopg2.extras import execute_values

POSTGRES_USERNAME = os.environ["POSTGRES_USERNAME"]
POSTGRES_PASSWORD = os.environ["POSTGRES_PASSWORD"]
POSTGRES_HOST = os.environ["POSTGRES_HOST"]
POSTGRES_PORT = os.environ["POSTGRES_PORT"]
POSTGRES_DATABASE = os.environ["POSTGRES_DATABASE"]
FILENAME = "tmp/predicted_records.json"

print(f"Reading file '{FILENAME}'")
with open(FILENAME) as file:
    file_content = file.read()

print(f"Parsing file '{FILENAME}'")
parsed_json = json.loads(file_content)

print(f"Converting JSON fields to str")
for element in parsed_json:
    element["versions_model"] = json.dumps(element.get("versions_model", {}))
    element["budgetary_values"] = json.dumps(element.get("budgetary_values", {}))

total_notices = len(parsed_json)
print(f"Number of notices to ingest : {total_notices}")

connection = psycopg2.connect(user=POSTGRES_USERNAME,
                              password=POSTGRES_PASSWORD,
                              host=POSTGRES_HOST,
                              port=POSTGRES_PORT,
                              database=POSTGRES_DATABASE)

i = 0
columns = parsed_json[0].keys()
BATCH_SIZE = 1000

for end_slice in range(BATCH_SIZE, total_notices + BATCH_SIZE, BATCH_SIZE):
    print("==================================")
    start_slice = end_slice - BATCH_SIZE
    iter_list = parsed_json[start_slice:end_slice]
    print(f"Current slice: '{start_slice}:{end_slice}' \t\t\t {100 * end_slice / total_notices}%")
    try:
        cursor = connection.cursor()
        query = "INSERT INTO aggregated_results ({}) VALUES %s".format(','.join(columns))
        values = [[value for value in notice.values()] for notice in iter_list]
        execute_values(cursor, query, values)
        connection.commit()
    except Exception as e:
        print(e)
        print(f"Exception at iteration: '{start_slice}:{end_slice}'")
        cursor.close()
        connection.close()
        sys.exit(1)
cursor.close()
connection.close()
