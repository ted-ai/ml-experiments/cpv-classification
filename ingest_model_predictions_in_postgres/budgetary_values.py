import re
from pathlib import Path
from typing import Any

import boto3
import spacy
from tika import parser

S3 = boto3.client('s3')
INPUT_BUCKET = "d-ew1-ted-ai-input"

BLACKLISTED_CHARS_IN_RESULT = ["+", ":", "<", ">", "/", "*", "=", "%", "&"]
RE_MULTIPLE_WHITESPACES_BETWEEN_DIGITS = r'(\d)\s+(\d)'
CUSTOM_CURRENCY_SYMBOLS = ["₣", "$", "€", "£", "¥", "₹", "د.ك"]
CUSTOM_CURRENCY_CODES = ['aed', 'afn', 'amd', 'ang', 'aoa', 'ars', 'aud', 'awg', 'afl', 'azn', 'bam', 'bdt', 'bbd',
                         'bgn', 'bhd', 'bif', 'bsd', 'bmd', 'bnd', 'bob', 'bov', 'brl', 'btn', 'bwp', 'byn', 'bzd',
                         'cad', 'cdf', 'che', 'chf', 'chw', 'clf', 'clp', 'cny', 'cop', 'cou', 'crc', 'cuc', 'cup',
                         'cve', 'czk', 'djf', 'dkk', 'dop', 'dzd', 'eek', 'egp', 'ern', 'etb', 'eur', 'fjd', 'fkp',
                         'gbp', 'gel', 'ggp', 'ghs', 'gip', 'gmd', 'gnf', 'gtq', 'gyd', 'hkd', 'hnl', 'hrk', 'htg',
                         'huf', 'idr', 'ils', 'imp', 'inr', 'iqd', 'irr', 'isk', 'jep', 'jmd', 'jod', 'jpy', 'kes',
                         'kgs', 'khr', 'kmf', 'kpw', 'krw', 'kwd', 'kyd', 'kzt', 'lak', 'lbp', 'lkr', 'lrd', 'lsl',
                         'ltl', 'lvl', 'lyd', 'mad', 'mdl', 'mga', 'mkd', 'mmk', 'mnt', 'mop', 'mro', 'mur', 'mvr',
                         'mwk', 'mxn', 'mxv', 'myr', 'mzn', 'nad', 'ngn', 'nio', 'nok', 'npr', 'prb', 'nzd', 'omr',
                         'pab', 'pen', 'pgk', 'php', 'pkr', 'pln', 'pyg', 'qar', 'ron', 'rsd', 'rub', 'rwf', 'sar',
                         'sbd', 'scr', 'sdg', 'sek', 'sgd', 'shp', 'sll', 'sos', 'srd', 'ssp', 'std', 'svc', 'syp',
                         'szl', 'thb', 'tjs', 'tmt', 'tnd', 'try', 'ttd', 'tvd', 'twd', 'tzs', 'uah', 'ugx', 'usd',
                         'usn', 'uyi', 'uyu', 'uzs', 'vef', 'vnd', 'vuv', 'wst', 'xaf', 'xag', 'xau', 'xba', 'xbb',
                         'xbc', 'xbd', 'xcd', 'xdr', 'xof', 'xpd', 'xpf', 'xpt', 'xsu', 'xts', 'xua', 'xxx', 'yer',
                         'zar', 'zmk', 'zmw', 'zwd', 'zwl', 'ntd', 'rmb']

MODEL: Any | None = None
PROCUREMENT_S3_KEYS = Path("tmp/procurement_s3_keys.txt").read_text().splitlines()


def load_model(path: str):
    global MODEL
    MODEL = spacy.load(path)


def predict(contract_id: str) -> list[str]:
    results = _run_prediction("\n".join(filter(None, (
        parser.from_buffer(S3.get_object(Bucket=INPUT_BUCKET, Key=k)['Body'].read()).get('content')
        for k in _filter_s3_keys(contract_id)
    ))))
    return [r["extracted_value"] for r in results]


def _filter_s3_keys(contract_id: str) -> list[str]:
    return [k for k in PROCUREMENT_S3_KEYS if f"/contract_id={contract_id}/" in k]


def _run_prediction(text: str):
    preprocessed_text = _preprocess_input_text(text)

    results = _get_budgetary_values_from_text(preprocessed_text)
    results_with_placeholder = []
    for element in results:
        placeholder_in_result = element[1].replace(element[0], "extractedbudgetaryvalue")
        results_with_placeholder.append([element[0], placeholder_in_result])
    filtered_results = []
    for index, result in enumerate(results_with_placeholder):
        nlp = MODEL
        probability = nlp(result[1]).cats["Contract"]
        if probability >= 0.5:
            filtered_results.append({"probability": probability, "extracted_value": result[0],
                                     "context": results[index][1]})
    return filtered_results


def _clean_str(input_string: str) -> str:
    str_en = input_string.encode("ascii", "ignore")
    str_de = str_en.decode()
    invalid_chars = ['—', '-', '\'', ',', '’', '–', '"', '|', '_', '`', '[', ']']
    for char in invalid_chars:
        str_de = str_de.replace(char, '')
    return ' '.join(str_de.split())


def _preprocess_input_text(input_text: str) -> str:
    input_text = _clean_str(input_text)
    input_text = input_text.lower()
    input_text = " ".join(input_text.split())
    input_text = re.sub(RE_MULTIPLE_WHITESPACES_BETWEEN_DIGITS, r'\1\2', input_text)
    return input_text


def _has_numbers(input_text: str) -> bool:
    return any(char.isdigit() for char in input_text)


def _is_valid_budget(text) -> bool:
    only_digits = re.sub("[^0-9]", "", text)
    if only_digits.startswith("0"):
        return False
    for blacklisted_char in BLACKLISTED_CHARS_IN_RESULT:
        if blacklisted_char in text:
            return False
    return True


def _get_budgetary_values_from_text(text: str) -> list[tuple[str, str]]:
    preprocessed_context = _preprocess_input_text(text)
    tokenized_context = preprocessed_context.split(" ")
    monetary_values = []
    context_length = 20
    for ind, element in enumerate(tokenized_context):
        if _has_numbers(element):
            for cur in CUSTOM_CURRENCY_CODES:
                if cur in element:
                    processed_tok = re.sub(r'\W+', '', element)  # Remove special characters
                    processed_tok = re.sub(r'\d+', '', processed_tok)  # Remove digits
                    if processed_tok == cur:
                        if _is_valid_budget(element):
                            context_index_min = max(0, ind - context_length)
                            context_index_max = min(len(tokenized_context), ind + context_length)
                            context = " ".join(tokenized_context[context_index_min:context_index_max])
                            monetary_values.append([element, context])
                else:
                    prev_index = ind - 1
                    if prev_index >= 0:
                        prev_token = tokenized_context[prev_index]
                        processed_tok = re.sub(r'\W+', '', prev_token)
                        if cur in prev_token and (
                                prev_token.endswith(cur) or prev_token.endswith(f"{cur}.")) and processed_tok == cur:
                            value_budget = f"{prev_token} {element}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind - context_length)
                                context_index_max = min(len(tokenized_context), ind + context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
                    next_index = ind + 1
                    if next_index < len(tokenized_context):
                        next_token = tokenized_context[next_index]
                        processed_tok = re.sub(r'\W+', '', next_token)
                        if cur in next_token and (next_token.startswith(cur) or next_token.startswith(
                                f".{cur}")) and processed_tok == cur:
                            value_budget = f"{element} {next_token}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind - context_length)
                                context_index_max = min(len(tokenized_context), ind + context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
            for cur in CUSTOM_CURRENCY_SYMBOLS:
                if cur in element:
                    processed_tok = re.sub(r'\d+', '', element)
                    processed_tok = processed_tok.replace(".", "")
                    processed_tok = processed_tok.replace(",", "")
                    if processed_tok == cur:
                        value_budget = f"{element}"
                        if _is_valid_budget(value_budget):
                            context_index_min = max(0, ind - context_length)
                            context_index_max = min(len(tokenized_context), ind + context_length)
                            context = " ".join(tokenized_context[context_index_min:context_index_max])
                            monetary_values.append([value_budget, context])
                else:
                    prev_index = ind - 1
                    if prev_index >= 0:
                        prev_token = tokenized_context[prev_index]
                        processed_tok = prev_token.replace(".", "")
                        processed_tok = processed_tok.replace(",", "")
                        if cur in processed_tok and processed_tok.endswith(cur):
                            value_budget = f"{processed_tok} {element}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind - context_length)
                                context_index_max = min(len(tokenized_context), ind + context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
                    next_index = ind + 1
                    if next_index < len(tokenized_context):
                        next_token = tokenized_context[next_index]
                        processed_tok = next_token.replace(".", "")
                        processed_tok = processed_tok.replace(",", "")
                        if cur in processed_tok and processed_tok.startswith(cur):
                            value_budget = f"{element} {processed_tok}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind - context_length)
                                context_index_max = min(len(tokenized_context), ind + context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])

    return monetary_values
