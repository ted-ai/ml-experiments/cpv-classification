CREATE TABLE IF NOT EXISTS aggregated_results
(
    id                                    BIGSERIAL,
    notice_id                             TEXT,
    contract_id                           TEXT,
    notice_type                           TEXT,
    title                                 TEXT,
    short_description                     TEXT,
    version                               TEXT,
    is_eu_institution                     BOOLEAN,
    cpvs                                  TEXT [],
    divisions                             TEXT [],
    divisions_opentender_model            TEXT [],
    divisions_linearsvc_model             TEXT [],
    divisions_roberta_model               TEXT [],
    budgetary_values                      JSON,
    procurement_document_budgetary_values TEXT [],
    versions_model                        JSON
);

ALTER TABLE aggregated_results SET UNLOGGED;
