# Script to extract data from Athena, make predictions from models deployed, and ingest data in RDS

## Pre-requisites

- Python3.10,
- Pip,
- AWS CLI profile setup,
- postgresql-devel installed.

## Dependencies

Install dependencies with following command:

```bash
pip3 install -r requirements.txt
```

## Scripts

1. `01_query_athena.py`: Execute this script to extract all notices with language being EN and having both a title and a
   short description. Output is available in file `tmp/athena_query_results.json`.
2. `02_list_procurement_docs.py`: Retrieve the list of procurement documents stored in the input bucket. Output is
   available in file `tmp/procurement_s3_keys.txt`.
3. `03_predict_from_models.py`: Execute this script to make CPV and budgetary values predictions on
   file `tmp/athena_query_results.json` with the 4 models deployed. Output is available in
   file `tmp/predicted_records.json`.
4. `04_ingest_data_in_postgres.py`: To execute this script , you first need to create the table `aggregated_results` if
   it does not exist. The schema is defined in `schema_table_aggregated_results.sql`. In addition, you have to export
   environment variables `POSTGRES_USERNAME`, `POSTGRES_PASSWORD`, `POSTGRES_HOST`, `POSTGRES_PORT` and
   `POSTGRES_DATABASE` in the terminal, and then execute this script to ingest records with predictions from the
   different model into postgres. If the table doesn't already exist in the DB, you need to run the following command:
   `PGPASSWORD="$POSTGRES_PASSWORD" psql --user "$POSTGRES_USERNAME" --host "$POSTGRES_HOST" --port "$POSTGRES_PORT" --dbname "$POSTGRES_DATABASE" -f schema_table_aggregated_results.sql`
5. To dump the table, your can
   run `PGPASSWORD="$POSTGRES_PASSWORD" pg_dump --table=aggregated_results --user "$POSTGRES_USERNAME" --host "$POSTGRES_HOST" --port "$POSTGRES_PORT" --dbname "$POSTGRES_DATABASE" > tmp/predicted_records.sql`
