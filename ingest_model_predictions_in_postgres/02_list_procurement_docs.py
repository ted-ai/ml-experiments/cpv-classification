from pathlib import Path
from typing import Iterator

import boto3

CLIENT = boto3.client("s3")


def main():
    Path("tmp/procurement_s3_keys.txt").write_text("\n".join(list_objects()))


def list_objects() -> Iterator[str]:
    continuation_token = None
    while True:
        params = {"Bucket": "d-ew1-ted-ai-input", "Prefix": "resource_type=procurement/"}
        if continuation_token:
            params['ContinuationToken'] = continuation_token
        response = CLIENT.list_objects_v2(**params)
        yield from [o["Key"] for o in response.get('Contents', [])]
        if response.get('NextContinuationToken'):
            continuation_token = response['NextContinuationToken']
        else:
            break


if __name__ == '__main__':
    main()
