import glob
import json
import os
import re
import tarfile
from typing import List, Dict, Any

import boto3
import joblib
import numpy as np
import spacy
import torch
from pydantic import BaseModel, Field, root_validator
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from unidecode import unidecode

import budgetary_values

# 1. Downloads models

print("Downloading models from S3")


def extract_all_files(tar_file_path, extract_to):
    with tarfile.open(tar_file_path, 'r') as tar:
        tar.extractall(extract_to)


BUCKET_MODELS_NAME = "d-ew1-ted-ai-ml-models"
OPENTENDER_MODEL_S3_KEY = "models/opentender-multi-label-division-classifier/v0.0.2/model.tar.gz"
LINEARSVC_MULTILABEL_CPV_CLASSIFIER_MODEL_S3_KEY = "models/multi-label-division-classifier/v0.0.5/model.tar.gz"
ROBERTA_MULTILABEL_CPV_CLASSIFIER_MODEL_S3_KEY = "models/roberta-multi-label-division-classifier/v0.0.1/model.tar.gz"
CONTRACT_BUDGETARY_VALUES_EXTRACTION_MODEL_S3_KEY = "models/contract-budgetary-values-extractor/v0.0.3/model.tar.gz"

OPENTENDER_MODEL_OUTPUT_NAME = "opentender_model.tar.gz"
LINEARSVC_MULTILABEL_CPV_CLASSIFIER__OUTPUT_NAME = "linearsvc_model.tar.gz"
ROBERTA_MULTILABEL_CPV_CLASSIFIER_MODEL_OUTPUT_NAME = "roberta_model.tar.gz"
CONTRACT_BUDGETARY_VALUES_EXTRACTION_MODEL_OUTPUT_NAME = "budgetary_values_model.tar.gz"
OUTPUT_MODELS_PATH = "tmp/models"

if not os.path.exists(OUTPUT_MODELS_PATH):
    os.makedirs(OUTPUT_MODELS_PATH)
else:
    files = glob.glob(f'{OUTPUT_MODELS_PATH}/**/*.*', recursive=True)
    for f in files:
        os.remove(f)

resource = boto3.resource('s3')
s3_bucket_models = resource.Bucket(BUCKET_MODELS_NAME)
versions_models = {}

for s3_key, output_filename in zip([OPENTENDER_MODEL_S3_KEY, LINEARSVC_MULTILABEL_CPV_CLASSIFIER_MODEL_S3_KEY,
                                    ROBERTA_MULTILABEL_CPV_CLASSIFIER_MODEL_S3_KEY,
                                    CONTRACT_BUDGETARY_VALUES_EXTRACTION_MODEL_S3_KEY],
                                   [OPENTENDER_MODEL_OUTPUT_NAME, LINEARSVC_MULTILABEL_CPV_CLASSIFIER__OUTPUT_NAME,
                                    ROBERTA_MULTILABEL_CPV_CLASSIFIER_MODEL_OUTPUT_NAME,
                                    CONTRACT_BUDGETARY_VALUES_EXTRACTION_MODEL_OUTPUT_NAME]):
    tar_local_path = f"{OUTPUT_MODELS_PATH}/{output_filename}"
    s3_bucket_models.download_file(s3_key, tar_local_path)
    extract_path = f"{OUTPUT_MODELS_PATH}/{output_filename.split('.')[0]}/"
    extract_all_files(tar_local_path, extract_path)
    os.remove(tar_local_path)
    version = s3_key.split('/')[-2]
    model_name = s3_key.split('/')[-3]
    versions_models[model_name] = version

print("Models downloaded from S3")

# 2. Load models
print("Loading models")

opentender_model = joblib.load(f"{OUTPUT_MODELS_PATH}/{OPENTENDER_MODEL_OUTPUT_NAME.split('.')[0]}/model.joblib")
linearsvc_model = joblib.load(
    f"{OUTPUT_MODELS_PATH}/{LINEARSVC_MULTILABEL_CPV_CLASSIFIER__OUTPUT_NAME.split('.')[0]}/model.joblib")
ROBERTA_PATH = f"{OUTPUT_MODELS_PATH}/{ROBERTA_MULTILABEL_CPV_CLASSIFIER_MODEL_OUTPUT_NAME.split('.')[0]}/roberta-cpv-multilabel/"
roberta_tokenizer = AutoTokenizer.from_pretrained(ROBERTA_PATH, local_files_only=True)
roberta_model = AutoModelForSequenceClassification.from_pretrained(ROBERTA_PATH)
budgetary_values.load_model(
    f"{OUTPUT_MODELS_PATH}/{CONTRACT_BUDGETARY_VALUES_EXTRACTION_MODEL_OUTPUT_NAME.split('.')[0]}/is_contract_budget")
print("Models loaded")

# 3. Load Athena data from local file
AVAILABLE_DIV = ['85', '44', '50', '80', '73', '45', '71', '79', '90', '30', '35', '33', '55', '72', '48', '38', '09',
                 '75', '66', '64', '42', '34', '60', '92', '39', '31', '98', '51', '32', '65', '77', '22', '63', '15',
                 '70', '18', '03', '24', '43', '19', '41', '37', '14', '16', '76']


class TableModel(BaseModel):
    notice_id: str = Field(alias='id')
    contract_id: str
    notice_type: str = Field(alias='type')
    title: str
    short_description: str
    cpvs: List[str] = []
    divisions: List[str] = []
    divisions_opentender_model: List[str] = []
    divisions_linearsvc_model: List[str] = []
    divisions_roberta_model: List[str] = []
    versions_model: Dict[str, str] = {}
    budgetary_values: List[Dict[str, Any]] = []
    procurement_document_budgetary_values: List[str] = []
    version: str
    is_eu_institution: bool

    @root_validator(pre=True)
    def _set_fields(cls, values: dict) -> dict:
        all_cpvs = values["additional_cpvs"].split(",")
        all_cpvs.append(values["main_cpv"])
        all_cpvs = [i for i in all_cpvs if i != ""]
        all_cpvs = list(set(all_cpvs))
        values["cpvs"] = all_cpvs
        all_divisions = []
        for single_cpv in all_cpvs:
            division = single_cpv[:2]
            if division in AVAILABLE_DIV and division not in all_divisions:
                all_divisions.append(division)
        values["divisions"] = all_divisions
        values["budgetary_values"] = json.loads(values["budgetary_values"])
        return values


print("Parsing Athena records")

with open('tmp/athena_query_results.json') as results_query_athena:
    file_contents = results_query_athena.read()

parsed_json = json.loads(file_contents)

parsed_object_table = [TableModel(**i) for i in parsed_json]
number_records_to_process = len(parsed_object_table)
print(f"Number records to process: {number_records_to_process}")

# 4. Preprocessing and prediction functions for each model

print("Downloading Spacy model")
spacy.cli.download("en_core_web_sm")
print("Loading Spacy model")
NLP = spacy.load("en_core_web_sm")
print("Spacy model loaded")

STOP_WORDS = NLP.Defaults.stop_words
CHARACTERS_TO_REPLACE = ["\\n", "\\r", "\\t", "\\W", "•", "\t", "-", "(", ")", ":", ";", "?", "!", "&", "\n", "\r", ".",
                         ",", "'", "’", "´",
                         "‘", "’", '"', "“", "”", "'", "/", "\\", "%", "—", "#", "$", "[", "]", "|", "{", "}", "~", "`",
                         "+", "*"]

MONTHS = [" january ", " february ", " march ", " april ", " may ", " june ", " july ", " august ", " september ",
          " october ", " november ", " december ",
          " jan ", " feb ", " mar ", " apr ", " jun ", " jul ", " aug ", " sep ", " oct ", " nov ", " dec "]


def _remove_multiple_spaces(text: str) -> str:
    return re.sub('\s+', ' ', text)


def _remove_special_characters(text: str) -> str:
    for chars in CHARACTERS_TO_REPLACE:
        text = text.replace(chars, " ")
    return text


def _remove_stop_words(text: str) -> str:
    token_list = text.split()
    removed_list = [x for x in token_list if x not in STOP_WORDS]
    return ' '.join(removed_list)


def _replace_digits(text):
    return re.sub(r'[\d-]+', 'NUMBER', text)


def _delete_one_letter_word(text):
    text_as_list = text.split()
    text_as_list = [element for element in text_as_list if len(element) > 1]
    return ' '.join(text_as_list)


def _remove_consecutive_duplicates(text):
    text_as_list = text.split()
    last_seen = None
    result = []
    for x in text_as_list:
        if x != last_seen:
            result.append(x)
        last_seen = x
    return ' '.join(result)


def _replace_months(text: str) -> str:
    text = " " + text + " "
    for month in MONTHS:
        text = text.replace(month, " MONTH ")
    return text


def _replace_with_lemma(text: str) -> str:
    doc = NLP(text)
    lemmatized_list = []
    for token in doc:
        lemmatized_list.append(token.lemma_)
    return " ".join(lemmatized_list)


def _preprocess_input(text: str) -> str:
    x = unidecode(str(text).lower())
    x = _replace_with_lemma(x)
    x = _remove_special_characters(x)
    x = _remove_stop_words(x)
    x = _remove_multiple_spaces(x)
    x = _replace_digits(x)
    x = _delete_one_letter_word(x)
    x = _remove_consecutive_duplicates(x)
    x = _replace_months(x)
    return x


with open(f"./{ROBERTA_PATH}config.json") as file:
    config_file = file.read()

ID2LABEL = json.loads(config_file)["id2label"]


def predict_roberta(tokenizer_model, prediction_model, title, description):
    if title:
        title = f'{title}. '
    else:
        title = ""
    if not description:
        description = ""

    title_description = f'{title}{description}'
    encoded_input = tokenizer_model(title_description, return_tensors='pt', padding=True, truncation=True)

    results = prediction_model(**encoded_input)
    logits = results.logits
    sigmoid = torch.nn.Sigmoid()
    probs = sigmoid(logits.squeeze().cpu())
    predictions = np.zeros(probs.shape)
    predictions[np.where(probs >= 0.5)] = 1
    predicted_labels = [ID2LABEL[str(idx)] for idx, label in enumerate(predictions) if label == 1.0]
    return predicted_labels


def predict_with_linear_svc_models(model_var, labels, preprocessed_text):
    predictions = model_var.predict(preprocessed_text)
    predictions_merged = predictions.max(axis=0)
    predictions_merged = predictions_merged.tolist()
    results = []
    for index, element in enumerate(predictions_merged):
        if element:
            cpv_number = labels[index]
            results.append(cpv_number)
    return results


# 5. Loop over each record and make predictions

ALL_CPVS_OPENTENDER_MODEL = ['71', '44', '50', '80', '73', '45', '85', '79', '90', '30', '35', '33', '55', '72',
                             '48', '38', '09', '75', '66', '64', '42', '34', '60', '92', '39', '31', '98', '51',
                             '77', '22', '32', '63', '15', '65', '70', '18', '03', '43', '24', '19', '41', '37',
                             '14', '16', '76']

ALL_CPVS_LINEAR_SVC_MODEL = ['85', '44', '50', '80', '73', '45', '71', '79', '90', '30', '35', '33', '55', '72', '48',
                             '38', '09', '75',
                             '66', '64', '42', '34', '60', '92', '39', '31', '98', '51', '32', '65', '77', '22', '63',
                             '15', '70', '18',
                             '03', '24', '43', '19', '41', '37', '14', '16', '76']

print("Predicting divisions")
i = 0
for element in parsed_object_table:
    i += 1
    preprocessed_title = _preprocess_input(element.title)
    preprocessed_description = _preprocess_input(element.short_description)
    preprocessed_title_description = f"{preprocessed_title} {preprocessed_description}"
    preprocessed_input = [preprocessed_title, preprocessed_description, preprocessed_title_description]
    predictions_linear_svc_model = predict_with_linear_svc_models(linearsvc_model, ALL_CPVS_LINEAR_SVC_MODEL,
                                                                  preprocessed_input)
    predictions_opentender_model = predict_with_linear_svc_models(opentender_model, ALL_CPVS_OPENTENDER_MODEL,
                                                                  preprocessed_input)
    predictions_roberta_model = predict_roberta(roberta_tokenizer, roberta_model, element.title,
                                                element.short_description)
    element.divisions_roberta_model = predictions_roberta_model
    element.divisions_linearsvc_model = predictions_linear_svc_model
    element.divisions_opentender_model = predictions_opentender_model
    element.versions_model = versions_models
    element.procurement_document_budgetary_values = budgetary_values.predict(element.contract_id)
    if i % 100 == 0:
        print(f'{100 * i / number_records_to_process}%')

output_formatted = [element.dict() for element in parsed_object_table]
json_object = json.dumps(output_formatted, indent=4)
with open("tmp/predicted_records.json", "w") as outfile:
    outfile.write(json_object)
