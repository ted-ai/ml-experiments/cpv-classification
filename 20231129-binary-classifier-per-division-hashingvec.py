import pickle
import sys
from multiprocessing import Pool
from pathlib import Path

import optuna
import pandas as pd
from sklearn import model_selection, metrics
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC


def list_divisions(df: pd.DataFrame) -> list[str]:
    return [c for c in df.columns if len(c) == 2 and c.isdigit()]


def evaluate_model(trial: optuna.Trial, model: Pipeline, pos_div: str, df: pd.DataFrame, df_name: str) -> float:
    predicted_df = model.predict(df["text"])
    scores = metrics.precision_recall_fscore_support(df[pos_div], predicted_df, average="binary")
    precision, recall, f1_score, _ = scores
    confusion_matrix = metrics.confusion_matrix(df[pos_div], predicted_df)
    trial.set_user_attr(f"{df_name}_precision", precision)
    trial.set_user_attr(f"{df_name}_recall", recall)
    trial.set_user_attr(f"{df_name}_f1_score", f1_score)
    trial.set_user_attr(f"{df_name}_true_negative_count", int(confusion_matrix[0][0]))
    trial.set_user_attr(f"{df_name}_false_positive_count", int(confusion_matrix[0][1]))
    trial.set_user_attr(f"{df_name}_false_negative_count", int(confusion_matrix[1][0]))
    trial.set_user_attr(f"{df_name}_true_positive_count", int(confusion_matrix[1][1]))
    return f1_score


def train_step(trial: optuna.Trial, pos_div: str) -> float:
    df = pd.read_csv("tmp/20231031-en-dataset-lvl12.csv", dtype={"id": str, "text": str})
    train_df, test_df = model_selection.train_test_split(df, test_size=0.2, random_state=0)
    del df
    vectorizer = HashingVectorizer(
        ngram_range=(1, trial.suggest_int("max_ngram_bound", 2, 3)),
        n_features=trial.suggest_int("n_features", 100_000, 2_000_000),
    )
    classifier = LinearSVC(
        max_iter=10000,
        C=trial.suggest_float("C", 1, 20),
        random_state=0,
        dual=True,
    )
    model = Pipeline([("tfidf", vectorizer), ("clf", classifier)])
    model.fit(train_df["text"], train_df[pos_div])
    trial.set_user_attr("model_size", sys.getsizeof(pickle.dumps(model)))
    evaluate_model(trial, model, pos_div, train_df, "train")
    return evaluate_model(trial, model, pos_div, test_df, "test")


def train_division(division: str):
    print(f"Start training division {division}...")
    study = optuna.create_study(direction="maximize")
    study.optimize(lambda trial: train_step(trial, division), n_trials=50)
    study.trials_dataframe().to_csv(f"tmp/20231121_training_results_hashingvec/trials_{division}.csv", index=False)
    print(f"Training of division {division} done.")


def main():
    Path("tmp/20231121_training_results_hashingvec").mkdir(exist_ok=True)
    all_divisions = list_divisions(pd.read_csv("tmp/20231031-en-dataset-lvl12.csv"))
    with Pool(45) as pool:
        pool.map(train_division, all_divisions)
    print("Done")


if __name__ == '__main__':
    main()
